import {combineReducers} from 'redux';
import user from './reduce_user';
import goals from './reduce_goals';
import completeGoals from './reduce_complete_goals';

export default combineReducers({
    user,goals,completeGoals
})
