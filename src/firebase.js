import * as firebase from 'firebase';

const config = {
    apiKey: "AIzaSyCBKF0_Nxf__hF_qtEkfxygslKP0Kzm4wc",
    authDomain: "goalcoach-a0d0f.firebaseapp.com",
    databaseURL: "https://goalcoach-a0d0f.firebaseio.com",
    projectId: "goalcoach-a0d0f",
    storageBucket: "goalcoach-a0d0f.appspot.com",
    messagingSenderId: "1064334799456"
  };

   export const firebaseApp = firebase.initializeApp(config);
   export const goalRef =firebase.database().ref('goals');
   export const completeGoalRef =firebase.database().ref('completeGoals');